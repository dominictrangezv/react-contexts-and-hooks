import React from 'react';
import { ReportingPageContentContainer, ReportingPageContent } from '../components/ReportingPageContent';
import { FirstNameBox } from '../components/FirstNameBox';
import { LastNameBox} from '../components/LastNameBox'

import { AnimalList } from '../components/AnimalList';
import { AnimalItem } from '../components/AnimalItem';
import { SubmitData } from '../components/SubmitData';

export const ReportingPage = () => {
  return (
    <ReportingPageContentContainer>
      <ReportingPageContent>
        <FirstNameBox />
        <LastNameBox />
        <AnimalList />
        <SubmitData />
      </ReportingPageContent>
    </ReportingPageContentContainer>
  )
}