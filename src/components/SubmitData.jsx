import React from 'react';
import { useAnimals } from '../providers/AnimalsProvider';
import { useReportForm } from '../providers/ReportFormProvider';

export const SubmitData = () => {

  const [animals] = useAnimals();
  const [reportForm] = useReportForm();

  return (
    <section>
      <div>Report Form</div>
      <code>
        {JSON.stringify(reportForm, null, 4)}
      </code>
      <div>Animals</div>
      <code>
        {JSON.stringify(animals, null, 4)}
      </code>

    </section>
  )

}