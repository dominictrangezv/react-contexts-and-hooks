import React, { useEffect } from 'react';
import { useReportForm } from '../providers/ReportFormProvider';
import { setLastName } from '../reducers/actions/setLastName';

export const LastNameBox = () => {

  let [{ lastName }, dispatch] = useReportForm();

  const onChangeHandler = (e) => {
    dispatch(setLastName(e.target.value));
  }

  useEffect(() => {
    console.log("LastNameBox -> Rerendering", lastName) 
  })

  return (
    <div>
      <label 
        for="lastNameInput"
      >
        Last name
      </label>
      <input 
        id="lastNameInput"
        onChange={onChangeHandler}
        value={lastName}
      />
    </div>
  )

}