import React, { useEffect } from 'react';
import { useReportForm } from '../providers/ReportFormProvider';
import { setFirstName } from '../reducers/actions/setFirstName';

export const FirstNameBox = () => {

  let [{ firstName }, dispatch] = useReportForm('firstName');

  const onChangeHandler = (e) => {
    dispatch(setFirstName(e.target.value));
  }

  useEffect(() => {
    console.log("FirstNameBox -> Rerendering") 
  })

  return (
    <div>
      <label 
        for="firstNameInput"
      >
        First name
      </label>
      <input 
        id="firstNameInput"
        onChange={onChangeHandler}
        value={firstName}
      />
    </div>
  )

}