import React, { useEffect, useState } from 'react';
import { ReportFormProvider } from '../providers/ReportFormProvider';
import { AnimalsProvider } from '../providers/AnimalsProvider';
import { FirstNameBox } from '../components/FirstNameBox';
import { LastNameBox} from '../components/LastNameBox'

export const ReportingPageContentContainer = ({ children }) => {
  return (
    <ReportFormProvider> 
      <AnimalsProvider>
        { children }
      </AnimalsProvider>
    </ReportFormProvider>
  )
}

export const ReportingPageContent = ({ children }) => {

  let [toggle, setToggle] = useState(false);

  useEffect(() => console.log("ReportingPageContent -> Rerendering"));

  return (
    <section>
      {children}
      {/* <FirstNameBox />
      <LastNameBox /> */}
      <button onClick={() => setToggle(!toggle)} >Toggle</button>
    </section>
  )
}