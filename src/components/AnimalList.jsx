import React, { useEffect } from 'react';
import { useAnimals } from '../providers/AnimalsProvider';
import { AnimalItem } from './AnimalItem';
import { addAnimal } from '../reducers/actions/addAnimal';

export const AnimalList = ({children}) => {
  
  const [animals, dispatch] = useAnimals();
  useEffect(() => {
    console.log('AnimalList -> Rerender')
  }, [])

  const addAnimalHandler = (e) => {
    dispatch(addAnimal("New Animal"));
  }
  
  const animalItems = Object.values(animals).map((animal) => {
    return (
      <AnimalItem 
        key={animal.id}
        { ...animal }
        dispatch={dispatch}
      />
    )
  })

  return (
    <section>
      <div>
        {animalItems}
      </div>
      <button onClick={addAnimalHandler}>
        Add animal
      </button>
    </section>
  )
}