import React, { memo, useEffect } from 'react';
import { updateAnimal } from '../reducers/actions/updateAnimal';

export const AnimalItem = memo(({id, name, dispatch}) => {
  
  useEffect(() => {
    console.log("AnimalItem -> Rerender")
  })

  const onChangeHandler = (e) => {
    dispatch(updateAnimal(id, e.target.value))
  }
  return (
    <div className="animalItem">
      <label for={`animal-${id}`}>Name</label>
      <input id={`animal-${id}`}
        value={name}
        onChange={onChangeHandler}
      />
    </div>
  )
})