export const AnimalsReducer = (state = {}, action) => {

  switch(action.type) {
    case "addAnimal":
      return {
        ...state,
        [action.payload.id] : action.payload
      }
    case "updateAnimal":
      return {
        ...state,
        [action.payload.id] : {
          ...state[action.payload.id],
          ...action.payload
        }
      }
    case "deleteAnimal":
      return {
        ...state
      }
    default:
      return state;
  }

}