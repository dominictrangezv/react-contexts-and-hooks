export const setFirstName = (firstName) => {
  return {
    type : "setFirstName",
    payload : firstName
  }
}