export const setLastName = (lastName) => {
  return {
    type : "setLastName",
    payload : lastName
  }
}