let id = 1;

export const addAnimal = (name) => {
  return {
    type : "addAnimal",
    payload : {
      id : id++,
      name : name
    }
  }
}