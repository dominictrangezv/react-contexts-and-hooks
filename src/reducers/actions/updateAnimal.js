export const updateAnimal = (id, name) => {
  return {
    type : "updateAnimal",
    payload : {
      id : id,
      name : name
    }
  }
}