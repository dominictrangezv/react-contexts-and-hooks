export const ReportFormReducer = (state, action) => {

  switch(action.type) {
    case "setFirstName":
      return {
        ...state,
        firstName : action.payload
      }
    case "setLastName":
      return {
        ...state,
        lastName : action.payload
      }
    default:
      return state;
  }

}