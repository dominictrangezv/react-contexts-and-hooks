import React from 'react';
import { ReportingPage } from './pages/ReportingPage';

function App() {
  return (
    <div className="App">
      <ReportingPage />
    </div>
  );
}

export default App;
