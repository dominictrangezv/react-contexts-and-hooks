import { createContext, useReducer, useContext } from 'react';
import { ReportFormReducer } from '../reducers/ReportFormReducer';

/**
 * Contains the value that will be returned by useReportForm
 */
const ReportFormContext = createContext(); 

/**
 * Treated like a component. This provides all children with access
 * to the context data
 */
export const ReportFormProvider = (props) => {

  let reportFormReducer = useReducer(ReportFormReducer, {firstName : "", lastName : ""})

  return (
    <ReportFormContext.Provider
      value={reportFormReducer}
      {...props}
    />
  )
}

/**
 * This hook is used in components to access the context data.
 * Note that this will fail if the component does not belong as a child
 * to the context provider
 */
export const useReportForm = () => {

  const reportFormContext = useContext(ReportFormContext);

  if (!reportFormContext) {
    throw new Error("ReportFormContext.Provider was not found");
  }

  return reportFormContext;

}