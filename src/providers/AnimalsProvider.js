import { createContext, useContext, useReducer } from 'react';
import { AnimalsReducer } from '../reducers/AnimalsReducer';

export const AnimalsContext = createContext(); 

export const AnimalsProvider = (props) => {

  let animalsReducer = useReducer(AnimalsReducer, {0 : { id : 0, name : "Dogmin"}})

  return (
    <AnimalsContext.Provider
      value={animalsReducer}
      {...props}
    />
  )

}

export const useAnimals = () => {
  
  const animalsContext = useContext(AnimalsContext);

  if (!animalsContext) {
    throw new Error("Animals.Provider was not found")
  }

  return animalsContext
}